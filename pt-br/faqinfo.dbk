<?xml version='1.0' encoding='utf-8'?>
<!-- -*- DocBook -*- -->
<chapter id="faqinfo"><title>Informações gerais sobre o FAQ.</title>
<section id="authors"><title>Autores</title>
<para>
A primeira edição deste FAQ foi feita e mantida por J.H.M.  Dassen (Ray) e
Chuck Stickelman.  Autores do reescrito Debian GNU/Linux FAQ são Susan G.
Kleinmann e Sven Rudolph.  Depois deles, o FAQ foi mantido por Santiago Vila.
O mantenedor atual é Josip Rodin.
</para>
<para>
Partes da informação vieram de:
</para>
<itemizedlist>
<listitem>
<para>
Do anúncio de lançamento do Debian-1.1, por <ulink
url="http://www.perens.com/">Bruce Perens</ulink>.
</para>
</listitem>
<listitem>
<para>
The Linux FAQ, por <ulink
url="http://www.chiark.greenend.org.uk/~ijackson/">Ian Jackson</ulink>.
</para>
</listitem>
<listitem>
<para>
<ulink url="http://lists.debian.org/">Arquivos das listas de discussão
Debian</ulink>,
</para>
</listitem>
<listitem>
<para>
O manual dos programadores do <command>dpkg</command> e o manual da política
do Debian (veja <xref linkend="debiandocs"/>)
</para>
</listitem>
<listitem>
<para>
muitos desevolvedores, voluntários e "beta testers", e ...
</para>
</listitem>
<listitem>
<para>
e as recordações de seus autores
</para>
</listitem>
</itemizedlist>
<para>
Os autores gostariam de agradecer todos aqueles que ajudaram a tornar este
documento possível.
</para>
<para>
Todas as garantias são negadas.  Todas as marcas registradas são propriedades
de seus respectivos donos.
</para>
</section>

<section id="feedback"><title>Feedback</title>
<para>
Comentários e adições a este documento sempre são bem-vindos.  Por favor,
envie um e-mail para <email>doc-debian@packages.debian.org</email>, ou submeta
um relatório de bugs (problemas) contra o pacote <systemitem
role="package">doc-debian</systemitem>.
</para>
</section>

<section id="latest"><title>Disponibilidade</title>
<para>
A mais recente versão deste documento pode ser vista nas páginas WWW do
Debian em <ulink
url="http://www.br.debian.org/doc/FAQ/">http://www.br.debian.org/doc/FAQ/</ulink>.
</para>
<para>
Também está disponível para download nos formatos texto simples, HTML e
PostScript no <ulink url="ftp://ftp.debian.org/debian/">servidor de FTP do
Debian</ulink> e em um de seus <ulink
url="http://www.debian.org/distrib/ftplist">espelhos</ulink> no diretório
<literal><ulink
url="ftp://ftp.debian.org/debian/doc/FAQ/">doc/FAQ/</ulink></literal>.
</para>
<para>
Os arquivos SGML originais, usados para criar este documento, estão
disponíveis no pacote fonte <systemitem
role="package">doc-debian</systemitem>, ou no servidor CVS em:
<literal>:pserver:anonymous@cvs.debian.org:/cvs/debian-doc/ddp/manuals.sgml/faq</literal>
</para>
</section>

<section id="docformat"><title>Formato do documento</title>
<para>
Este documento foi escrito usando o DebianDoc SGML DTD (reescrito do SGML em
LinuxDoc).  O sistema DebianDoc SGML nos permite criar arquivos em uma
variedade de formatos a partir e de um original, por exemplo,este documento
pode ser visto como HTLM, texto simples, TeX DVI, PostScript, PDF ou GNU info.
</para>
<para>
Utilitários de conversão para DebianDoc SGML estão disponíveis no pacote
<systemitem role="package">debiandoc-sgml</systemitem>.
</para>
</section>

</chapter>

