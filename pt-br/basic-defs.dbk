<?xml version='1.0' encoding='utf-8'?>
<!-- -*- DocBook -*- -->
<chapter id="basic-defs"><title>Visão geral e definições.</title>
<section id="whatisdebian"><title>O que é o Debian GNU/Linux?</title>
<para>
Debian GNU/Linux é uma <emphasis>distribuição</emphasis> particular do
sistema operacional Linux, e numerosos pacotes que rodam nele.
</para>
<para>
Em princípio, os usuários poderiam obter o kernel (núcleo do sistema
operacional) do Linux através da Internet ou de qualquer outro lugar e
compilá-lo por si próprios.  Eles poderiam então, obter o código fonte de
vários aplicativos da mesma maneira, compilar os programas e então,
instalá-los em seus sistemas.  Para programas complexos, este processo pode
ser não apenas demorado, mas também, suscetível a erros.  Para evitar isto,
os usuários freqüentemente escolhem obter o sistema operacional e os pacotes
de aplicativos de um dos distribuidores Linux.  O que distingue os vários
distribuidores Linux são os programas, protocolos e as práticas usadas para
empacotar, instalar e monitorar pacotes de aplicativos nos sistemas dos
usuários, combinados com a instalação e as ferramentas de manutenção,
documentação e outros serviços.
</para>
<para>
O Debian GNU/Linux é o resultado de um esforço voluntário para criar um
sistema operacional livre, de alta qualidade, compatível com Unix e completo,
com um conjunto de aplicativos.  A idéia de um sistema livre compatível com o
Unix originou-se do projeto GNU, e muitos dos aplicativos que fazem do Debian
GNU/Linux tão útil, foram desenvolvidos pelo projeto GNU.
</para>
<para>
Para o Debian, <emphasis>livre</emphasis> tem o significado definido pela GNU
(veja <ulink url="http://www.br.debian.org/social_contract#guidelines">As
Linhas-Guia do Software Livre Debian</ulink>).  Quando falamos de software
livre, estamos nos referindo a liberdade e não ao preço.  <emphasis>Software
livre</emphasis> significa que você tem a liberdade de distribuir cópias de
software livre, que você recebe o código-fonte ou pode adquiri-lo se quiser,
que você pode alterar o software ou usar partes dele em novos programas
livres; e para saber que você pode fazer estas coisas.
</para>
<para>
O Projeto Debian foi criado por Ian Murdock em 1993, inicialmente patrocinado
pelo projeto GNU da Free Software Foundation.  Hoje, os projetistas do Debian
entendem-no como um descendente direto do projeto GNU.
</para>
<para>
O Debian GNU/Linux é:
</para>
<itemizedlist>
<listitem>
<para>
<emphasis role="strong">completo</emphasis>: O Debian atualmente inclui mais de
42551 pacotes de software.  Os usuários podem escolher quais pacotes instalar;
o Debian provê uma ferramenta para este propósito.  Você pode encontrar uma
lista e descrições dos pacotes atualmente disponíveis no Debian em qualquer
um dos <ulink url="http://www.br.debian.org/distrib/ftplist.html">sites
espelhos do Debian</ulink>.
</para>
</listitem>
<listitem>
<para>
<emphasis role="strong">livre para usar e redistribuir</emphasis>: Não é
exigido nenhum pagamento para participar de sua distribuição e
desenvolvimento.  Todos os pacotes que são formalmente parte do Debian
GNU/Linux são livres para redistribuição, geralmente sob os termos
especificados pela Licença Pública Geral GNU (GNU General Public License).
</para>
<para>
Os repositórios FTP do Debian também contêm aproximadamente 696 pacotes de
software (nos diretórios <literal>non-free</literal> e
<literal>contrib</literal>), que são distribuídos sob os termos específicos
incluídos em cada pacote.
</para>
</listitem>
<listitem>
<para>
<emphasis role="strong">dinâmico</emphasis>: com aproximadamente 1033
voluntários contribuindo constantemente com novos e melhores códigos, o
Debian está evoluindo rapidamente.  Novas versões são periodicamente
planejadas com alguns meses de intervalo, e os repositórios FTP são
atualizados diariamente.
</para>
</listitem>
</itemizedlist>
<para>
Embora o próprio Debian GNU/Linux seja software livre, ele é uma base sobre a
qual novas distribuições podem ser construídas.  Proporcionando um sistema
básico confiável e completo, o Debian proporciona aos usuários de Linux
maior compatibilidade, e permite que criadores de distribuições Linux
eliminem a duplicação de esforços e se concentrem nas coisas que tornam suas
distribuições especiais.  veja <xref linkend="childistro"/> para mais
informações.
</para>
</section>

<section id="linux"><title>OK, agora eu sei o que é o Debian...o que é Linux!?</title>
<para>
Resumidamente, Linux é o kernel (núcleo) de um sistema operacional estilo
Unix.  Ele foi originalmente projetado para PCs 386 (e melhores); agora,
suporte a outros sistemas, incluindo os sistemas multi-processados, estão em
desenvolvimento.  O Linux é desenvolvido por Linus Torvalds e por vários
cientistas de computação ao redor do mundo.
</para>
<para>
Além do kernel, um sistema "Linux" geralmente possui:
</para>
<itemizedlist>
<listitem>
<para>
um sistema de arquivos que segue o Padrão de Hierarquia para o Sistema de
Arquivos Linux (<ulink url="http://www.pathname.com/fhs/">Linux Filesystem
Hierarchy Standard</ulink>).
</para>
</listitem>
<listitem>
<para>
uma vasta gama de utilitários Unix, muitos dos quais foram desenvolvidos pelo
projeto GNU e pela Free Software Foundation.
</para>
</listitem>
</itemizedlist>
<para>
A combinação do kernel Linux, do sistema de arquivos, dos utilitários GNU e
FSF, e dos outros utilitários, são projetados para alcançar conformidade com
o padrão POSIX (IEEE 1003.1); veja <xref linkend="otherunices"/>.
</para>
<para>
Para maiores informações sobre o Linux, veja a <ulink
url="ftp://sunsite.unc.edu/pub/Linux/docs/HOWTO/INFO-SHEET">INFO-SHEET</ulink>
de Michael K.  Johnson e o <ulink
url="ftp://sunsite.unc.edu/pub/Linux/docs/HOWTO/META-FAQ">META-FAQ</ulink>.
</para>
</section>

<section id="hurd"><title>O que é essa coisa chamada "Hurd"?</title>
<para>
O Hurd é um conjunto de servidores que rodam sobre o microkernel GNU Mach.
Juntos, eles constroem a base para o sistema operacional GNU.
</para>
<para>
Atualmente, o Debian está apenas disponível para Linux, mas com o Debian
GNU/Hurd, nós começamos a oferecer o Hurd como um desenvolvimento, servidor e
plataforma, também.  Porém, o Debian GNU/Hurd não foi lançado oficialmente
ainda, e não será ainda por algum tempo.
</para>
<para>
Por favor, veja <ulink
url="http://www.gnu.org/software/hurd/">http://www.gnu.org/software/hurd/</ulink>
para mais informações sobre o GNU/Hurd em geral, e <ulink
url="http://www.br.debian.org/ports/hurd/">http://www.br.debian.org/ports/hurd/</ulink>
para mais informações sobre o Debian GNU/Hurd.
</para>
</section>

<section id="difference"><title>Qual é a diferença entre o Debian GNU/Linux e outras distribuições Linux? Por que eu deveria escolher o Debian ao invés de outra distribuição?</title>
<para>
Estas características chaves distinguem o Debian de outras distribuições
Linux:
</para>
<variablelist>
<varlistentry>
<term>O sistema de manutenção de pacotes Debian:</term>
<listitem>
<para>
O sistema inteiro, ou qualquer componente individual dele, pode ser atualizado
sem necessidade de reformatação, sem perda de arquivos de configuração
personalizados e (na maioria dos casos) sem reinicializar o sistema.  A maioria
das distribuições Linux disponíveis hoje, têm algum tipo de sistema de
manutenção de pacotes; o sistema de manutenção de pacotes Debian é único
e particularmente robusto.  (veja <xref linkend="pkg-basics"/>)
</para>
</listitem>
</varlistentry>
<varlistentry>
<term>Desenvolvimento aberto:</term>
<listitem>
<para>
Considerando que outras distribuições Linux são desenvolvidas por
indivíduos, grupos pequenos e fechados ou entidades comerciais, o Debian é a
única distribuição Linux que está sendo desenvolvida de forma cooperativa
por vários indivíduos através da Internet, no mesmo espírito do Linux e de
outros programas livres.
</para>
<para>
Mais de 1033 mantenedores voluntários de pacotes estão trabalhando em mais de
42551 pacotes e melhorando o Debian GNU/Linux.  Os desenvolvedores Debian
contribuem para o projeto não apenas criando novos aplicativos (na maioria dos
casos), mas empacotando o software existente de acordo com os padrões do
projeto, enviando relatórios de bugs (falhas nos programas) para os
desenvolvedores oficiais e provendo apoio ao usuário.  Veja informações
adicionais em <xref linkend="contrib"/>.
</para>
</listitem>
</varlistentry>
<varlistentry>
<term>O Sistema de Monitoramento de Bugs:</term>
<listitem>
<para>
A dispersão geográfica dos desenvolvedores Debian exigiu sofisticadas
ferramentas e comunicação rápida de bugs e suas soluções para acelerar o
desenvolvimento do sistema.  Os usuários são estimulados a enviar bugs em um
estilo formal, que é rapidamente acessível pela WWW ou via e-mail.  Veja
informações adicionais nesta FAQ (Frequently Asked Questions - Perguntas
Feitas Freqüentemente) sobre a administração do <xref linkend="buglogs"/>.
</para>
</listitem>
</varlistentry>
<varlistentry>
<term>A Política do Debian:</term>
<listitem>
<para>
O Debian possui uma especificação extensa de nossos padrões de qualidade, a
Política do Debian (Debian Policy).  Esse documento define as qualidades e
padrões aos quais atrelamos os pacotes Debian.
</para>
</listitem>
</varlistentry>
</variablelist>
<para>
Para informações adicionais sobre isto, por favor, veja nossa página web
sobre <ulink url="http://www.br.debian.org/intro/why_debian">razões para
escolher o Debian</ulink>.
</para>
</section>

<section id="gnu"><title>Como o projeto Debian se encaixa (no) ou se compara com o projeto GNU da Free Software Foundation?</title>
<para>
O sistema Debian foi baseado nos ideais de software livre originalmente
defendidos pela <ulink url="http://www.gnu.org/">Free Software
Foundation</ulink> e em particular por Richard Stallman.  As poderosas
ferramentas de desenvolvimento, utilitários e aplicativos da FSF são também
parte importante do sistema Debian.
</para>
<para>
O Projeto Debian é uma entidade separada da FSF, porém, nos comunicamos
regularmente e cooperamos em vários projetos.  A FSF pediu explicitamente que
chamássemos nosso sistema de "Debian GNU/Linux", e ficamos contentes em
atender a este pedido.
</para>
<para>
O objetivo de longo prazo da FSF é desenvolver um novo sistema operacional
chamado GNU, baseado no HURD (<ulink
url="http://www.gnu.org/software/hurd/hurd.html">http://www.gnu.org/software/hurd/hurd.html</ulink>).
</para>
</section>

<section id="pronunciation"><title>Como se pronuncia Debian e o que essa palavra significa?</title>
<para>
O nome do projeto é pronunciado como Deb'-ian, com ênfase na primeira
sílaba.  Esta palavra é uma contração dos nomes Debra e Ian Murdock, sendo
este último, o fundador do projeto.  (Dicionários parecem oferecer alguma
ambigüidade na pronúncia de Ian, mas o mesmo prefere ee'-um)
</para>
</section>

</chapter>

