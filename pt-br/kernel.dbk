<?xml version='1.0' encoding='utf-8'?>
<!-- -*- DocBook -*- -->
<chapter id="kernel"><title>Debian e o kernel.</title>
<section id="non-debian-kernel"><title>Posso instalar e compilar um kernel sem alguma configuração específica para o Debian?</title>
<para>
Sim.  Mas você terá que entender a política do Debian em relação aos
cabeçalhos.
</para>
<para>
As bibliotecas C do Debian são construídas com as versões
<emphasis>estáveis</emphasis> mais recentes dos cabeçalhos do <emphasis
role="strong">kernel</emphasis>.
</para>
<para>
Por exemplo, a versão Debian-1.2 usava a versão 5.4.13 dos cabeçalhos.  Esta
prática contrasta com os pacotes fonte do kernel Linux, distribuídos em todos
os sites de FTP, que usam até mesmo as mais recentes versões dos cabeçalhos.
Os cabeçalhos do kernel, distribuídos com os fontes do kernel, estão
localizados em <literal>/usr/include/linux/include/</literal>.
</para>
<para>
Se você precisa compilar um programa com os cabeçalhos do kernel que são
mais atuais do que aqueles providos por <systemitem
role="package">libc6-dev</systemitem>, então você terá que adicionar
<literal>-I/usr/src/linux/include/</literal> na sua linha de comando quando
estiver compilando.  Isto veio em um ponto, por exemplo, com o empacotamento do
"automounter daemon" (<systemitem role="package">amd</systemitem>).  Quando
novos kernels mudaram algo em relação ao tratamento interno a NFS, o
<literal>amd</literal> precisou saber sobre eles.  Isto necessitou a inclusão
dos mais recentes cabeçalhos do kernel.
</para>
</section>

<section id="customkernel"><title>Que ferramentas o Debian oferece para a construção de kernels personalizados?</title>
<para>
Usuários que desejarem (ou precisarem) construir um kernel personalizado são
incentivados a baixar o pacote <systemitem
role="package">kernel-package</systemitem>.  Este pacote contém os scripts
para construir o pacote do kernel, e oferece a capacidade de criar um pacote
imagem do kernel somente rodando o comando
</para>
<screen>
make-kpkg kernel_image
</screen>
<para>
no diretório de mais alto nível dos fontes do kernel.  Ajuda é obtida
executando o comando
</para>
<screen>
make-kpkg --help
</screen>
<para>
e através da página de manual
<citerefentry><refentrytitle>make-kpkg</refentrytitle><manvolnum>1</manvolnum></citerefentry>.
</para>
<para>
Usuários devem baixar separadamente o código fonte do kernel mais recente (ou
de seu kernel preferido) do seu repositório Linux favorito, a menos que um
pacote kernel-source-versão esteja disponível (onde "version" significa a
versão do kernel).
</para>
<para>
Instruções detalhadas sobre o uso do pacote <systemitem
role="package">kernel-package</systemitem> são dadas no arquivo
<filename>/usr/share/doc/kernel-package/README.gz</filename>.  Resumidamente, a
pessoa deve:
</para>
<itemizedlist>
<listitem>
<para>
Desempacotar os fontes do kernel, e dar <literal>cd</literal> para o diretório
recém-criado.
</para>
</listitem>
<listitem>
<para>
Modificar a configuração do kernel usando um destes comandos:
</para>
<itemizedlist>
<listitem>
<para>
<literal>make config</literal> (para uma interface tty uma-linha-de-cada-vez).
</para>
</listitem>
<listitem>
<para>
<literal>make menuconfig</literal> (para uma interface de menus baseada em
ncurses).  Observe que, para usar esta opção, o pacote <systemitem
role="package">libncurses5-dev</systemitem> deve estar instalado.
</para>
</listitem>
<listitem>
<para>
<literal>make xconfig</literal> (para uma interface X11).  Usar esta opção
requer que os pacotes relevantes ao X e ao Tcl/Tk sejam instalados.
</para>
</listitem>
</itemizedlist>
<para>
Qualquer dos passos acima gera um novo <literal>.config</literal> no diretório
do fonte do kernel.
</para>
</listitem>
<listitem>
<para>
Execute o comando: <literal>make-kpkg -rev Custom.N kernel_image</literal>,
onde N é um número de revisão designado pelo usuário.  O novo arquivo
Debian assim formado teria revisão Custom.1, por exemplo,
<literal>kernel-image-2.2.14_Custom.1_i386.deb</literal> para o kernel Linux
2.2.14.
</para>
</listitem>
<listitem>
<para>
Instale o pacote criado.
</para>
<itemizedlist>
<listitem>
<para>
Rode <literal>dpkg --install /usr/src/kernel-image-VVV_Custom.N.deb</literal>
para instalar o kernel propriamente dito.  O script de instalação irá:
</para>
<itemizedlist>
<listitem>
<para>
executar o boot loader, LILO (se estiver instalado),
</para>
</listitem>
<listitem>
<para>
instalar o kernel personalizado em /boot/vmlinuz_VVV-Custom.N, e configurar as
ligações simbólicas apropriadas ao kernel mais recente.
</para>
</listitem>
<listitem>
<para>
sugerir ao usuário que crie um disquete de inicialização.  Este disquete
conterá apenas o kernel.  Veja <xref linkend="custombootdisk"/>.
</para>
</listitem>
</itemizedlist>
</listitem>
<listitem>
<para>
Para usar um boot loader secundário como o <systemitem
role="package">grub</systemitem> ou <systemitem
role="package">loadlin</systemitem>, copie essa imagem para outros lugares (por
exemplo, para /boot/grub ou para uma partição <literal>MS-DOS</literal>).
</para>
</listitem>
</itemizedlist>
</listitem>
</itemizedlist>
</section>

<section id="custombootdisk"><title>Como faço um disquete de inicialização personalizado?</title>
<para>
Essa tarefa é muito facilitada pelo pacote Debian <systemitem
role="package">boot-floppies</systemitem>, normalmente encontrado na seção
<literal>admin</literal> do repositório FTP do Debian.  Scripts de shell desse
pacote produzem disquetes de inicialização no formato
<literal>SYSLINUX</literal>.  Esses disquetes são formatados como
<literal>MS-DOS</literal> cujos MBRs foram alterados para inicializarem o Linux
diretamente (ou qualquer outro sistema operacional que tenha sido definido no
arquivo syslinux.cfg do disquete).  Outros scripts desse pacote produzem discos
root de emergência e podem até reproduzir os "base disks" (os discos do
sistema básico).
</para>
<para>
Você achará mais informações sobre isto no arquivo
<literal>/usr/doc/boot-floppies/README</literal> após instalar o pacote
<systemitem role="package">boot-floppies</systemitem>.
</para>
</section>

<section id="modules"><title>Que ferramentas especiais o Debian oferece para lidar com módulos?</title>
<para>
O pacote Debian <systemitem role="package">modconf</systemitem> fornece um
script shell (<literal>/usr/sbin/modconf</literal>) que pode ser usado para
personalizar a configuração dos módulos.  Este script apresenta uma
interface baseada em menus, requisitando ao usuário detalhes sobre os drivers
de dispositivos carregáveis de seu sistema.  As respostas são usadas para
personalizar o arquivo <literal>/etc/modules.conf</literal> que lista "aliases"
(apelidos), e outros argumentos (que devem ser usados em conjunto com vários
módulos) através de arquivos em <literal>/etc/modules</literal> (que lista os
módulos que devem ser carregados na hora do boot).
</para>
<para>
Como os (novos) arquivos Configure.help, que agora estão disponíveis para
auxiliar a construção de kernels personalizados, o pacote <systemitem
role="package">modconf</systemitem> vem com uma série de arquivos de ajuda (em
<literal>/usr/lib/modules_help/</literal>) que fornecem informações
detalhadas sobre os argumentos apropriados para cada um dos módulos.
</para>
</section>

<section id="removeoldkernel"><title>Posso desinstalar um kernel antigo de forma segura? Como?</title>
<para>
Sim.  O script <literal>kernel-image-NNN.prerm</literal> verifica se o kernel
que você está usando atualmente é o mesmo que você está querendo
desinstalar.  Portanto, você pode remover pacotes de kernel indesejáveis
usando este comando:
</para>
<screen>
dpkg --purge --force-remove-essential kernel-image-NNN
</screen>
<para>
(claro, substitua "NNN" com sua versão de kernel e número de revisão)
</para>
</section>

</chapter>

