<?xml version='1.0' encoding='utf-8'?>
<!-- -*- DocBook -*- -->
<chapter id="ftparchives"><title>Los archivos FTP de Debian</title>
<section id="dirtree"><title>¿Qué son todos esos directorios en los archivos de FTP de Debian?</title>
<para>
El software que se ha empaquetado para Debian GNU/Linux está disponible en
varios árboles de directorios de cada espejo de Debian.  El directorio
<literal>dists</literal> contiene las "distribuciones", y es ahora la forma
canónica de acceder a ellas.
</para>
<para>
El directorio <literal>pool</literal> contiene los paquetes de verdad.
</para>
<para>
Existen los siguientes directorios suplementarios:
</para>
<variablelist>
<varlistentry>
<term><emphasis>/tools/</emphasis>:</term>
<listitem>
<para>
Utilidades DOS para crear discos de arranque, particionar el disco duro,
comprimir y descomprimir archivos, y arrancar Linux.
</para>
</listitem>
</varlistentry>
<varlistentry>
<term><emphasis>/doc/</emphasis>:</term>
<listitem>
<para>
Documentación básica sobre Debian, como la FAQ, y las instrucciones sobre
cómo enviar informes de bugs.
</para>
</listitem>
</varlistentry>
<varlistentry>
<term><emphasis>/indices/</emphasis>:</term>
<listitem>
<para>
El fichero Maintainers y los ficheros override.
</para>
</listitem>
</varlistentry>
<varlistentry>
<term><emphasis>/project/</emphasis>:</term>
<listitem>
<para>
material para desarrolladores principalmente, como:
</para>
<variablelist>
<varlistentry>
<term><emphasis>project/experimental/</emphasis>:</term>
<listitem>
<para>
Este directorio contiene paquetes y utilidades que se están desarrollando
específicamente para el Proyecto Debian, y que aún están en fase de pruebas.
Los usuarios no deberían usar paquetes de aquí, porque pueden ser peligrosos
y perjudiciales incluso para alguien experimentado.
</para>
</listitem>
</varlistentry>
</variablelist>
</listitem>
</varlistentry>
</variablelist>
</section>

<section id="dists"><title>¿Cuántas distribuciones de Debian hay en el directorio <literal>dists</literal>?</title>
<para>
Normalmente hay tres distribuciones, la distribución "stable" (estable), la
distribución "testing" (en pruebas), y la distribución "unstable"
(inestable).
</para>
</section>

<section id="codenames"><title>¿Qué son todos esos nombres como slink, potato, etc.?</title>
<para>
Son simplemente nombres clave.  Cuando una distribución de Debian está en su
fase de desarrollo, no tiene número de versión, solamente tiene un nombre
clave.  El objeto de estos nombres clave es hacer que las distribuciones de
Debian sean más fáciles de replicar (si un directorio real tal y como
<literal>unstable</literal> cambiara repentinamente de nombre a
<literal>stable</literal>, habría que volver a traerse un montón de Megabytes
de nuevo).
</para>
<para>
Actualmente, <literal>stable</literal> es un enlace simbólico a
<literal>stretch</literal> (o sea, Debian 9), y <literal>testing</literal> es
un enlace simbólico a <literal>buster</literal>, lo cual significa que
<literal>stretch</literal> es la distribución "stable" actual y
<literal>buster</literal> es la distribución "testing" actual.
</para>
<para>
<literal>unstable</literal> es un enlace simbólico permanente a
<literal>sid</literal>, dado que <literal>sid</literal> es siempre la
distribución unstable.
</para>
<section id="oldcodenames"><title>¿Qué otros nombres clave se han utilizado ya?</title>
<para>
Otros nombres clave que ya se han utilizado son: <literal>buzz</literal> para
Debian 1.1, <literal>rex</literal> para Debian 1.2, <literal>bo</literal> para
Debian 1.3.x, <literal>hamm</literal> para Debian 2.0, <literal>slink</literal>
para Debian 2.1, <literal>potato</literal> para Debian 2.2, y
<literal>woody</literal> para Debian 3.0.
</para>
</section>

<section id="sourceforcodenames"><title>¿De dónde provienen estos nombres clave?</title>
<para>
Hasta ahora han sido personajes de la película Toy Story, de los estudios de
animación Pixar.
</para>
</section>

</section>

<section id="sid"><title>¿Y qué es "sid"?</title>
<para>
Es una distribución especial para arquitecturas que todavía no han sido
publicadas por primera vez.
</para>
<para>
Cuando sid no existía, la organización del FTP tenía un fallo: Había una
presunción de que cuando se creaba una nueva arquitectura en unstable, sería
publicada cuando esa distribución se convertía en la nueva stable.  Para
muchas arquitecturas este no es el caso, lo cual resultaba en que esos
directorios tenían que ser cambiados de lugar en el momento de la
publicación, desperdiciando un montón de ancho de banda.
</para>
<para>
Para esas arquitecturas todavía no publicadas, la primera vez que se publiquen
habrá un enlace desde el stable actual hasta sid, y de ahí en adelante se
crearán dentro del árbol unstable de forma normal.  sid no será publicado
nunca ni siquiera se accederá a él directamente, solamente a través de
enlaces simbólicos en los árboles actuales stable, frozen y unstable.  Será
una mezcla de arquitecturas publicadas y no publicadas.
</para>
<para>
El nombre de "sid" tambien proviene de la película "Toy Story", era el chico
de al lado que rompía los juguetes :-)
</para>
</section>

<section id="stable"><title>¿Qué contiene el directorio stable?</title>
<itemizedlist>
<listitem>
<para>
stable/main/: Este directorio contiene los paquetes que formalmente constituyen
la distribución más reciente del sistema Linux de Debian.
</para>
</listitem>
<listitem>
<para>
stable/non-free/: Este directorio contiene paquetes cuya distribución está
restringida en un modo que obliga a que los distribuidores tengan muy presente
ciertos requisitos de copyright.  Por ejemplo, algunos paquetes tienen
licencias que prohiben la distribución comercial.  Otros pueden ser
distribuidos, aunque de hecho son shareware, y no freeware.  Las licencias de
cada uno de estos paquetes debe ser estudiada, y posiblemente negociada, antes
de que los paquetes se incluyan en cualquier redistribución (p.ej., en un
CD-ROM).
</para>
</listitem>
<listitem>
<para>
stable/contrib/: Este directorio contiene paquetes que son de <emphasis>libre
distribución</emphasis>, pero que no cumplen con la política de requisitos de
distribución de los paquetes del Proyecto Debian por alguna razón, p.ej., los
paquetes tienen alguna modificación o restricción inusual, o sólo están
disponibles en formato binario.  Para estos paquetes, el proyecto no puede
ofrecer al usuario ninguna forma de asegurarse de que están libres de Caballos
de Troya, y no puede adaptarlos a otras arquitecturas.  Paquetes sólo en
binario que no sean de libre distribución se encuentran en el directorio
<literal>non-free</literal>.
</para>
</listitem>
</itemizedlist>
<section id="s5.5.1"><title>¿Qué contiene el directorio unstable?</title>
<para>
unstable contiene una muestra del sistema actual bajo desarrollo.  Se invita a
los usuarios a probar estos paquetes, aunque se les advierte sobre su estado
incompleto.  También hay directorios main, contrib y non-free dentro de
unstable.
</para>
</section>

<section id="s5.5.2"><title>¿Qué son todos esos directorios dentro de <literal>dists/stable/main</literal>?</title>
<para>
Dentro de cada uno de los directorios principales
(<literal>dists/stable/main</literal>, <literal>dists/stable/contrib</literal>,
<literal>dists/stable/non-free</literal>, y
<literal>dists/unstable/main/</literal>, etc.  pero no
<literal>project/experimental/</literal>, que es demasiado pequeño para ser
subdividido), los paquetes binarios residen en subdirectorios cuyos nombres
indican la arquitectura del chip para el que fueron compilados:
</para>
<itemizedlist>
<listitem>
<para>
binary-all, para paquetes que son independientes de la arquitectura.  Esto
incluye, por ejemplo, scripts en Perl.
</para>
</listitem>
<listitem>
<para>
binary-i386, para paquetes que sólo se ejecutan en máquinas 80x86.
</para>
</listitem>
<listitem>
<para>
binary-m68k, para paquetes que se ejecutan en máquinas basadas en uno de los
procesadores Motorola 680x0.  Actualmente se mantiene para computadoras Atari y
Amiga, y para algunas placas industriales basadas en VME.  No hay una
implementación de Linux para los Macintosh basados en el antiguo m68k, porque
Apple no suministró la información de hardware necesaria.
</para>
</listitem>
<listitem>
<para>
binary-sparc/, para paquetes que se ejecutan en Sparcstations de Sun.
</para>
</listitem>
<listitem>
<para>
binary-alpha/, para paquetes que se ejecutan en máquinas DEC de Alpha.
</para>
</listitem>
<listitem>
<para>
binary-powerpc/, para paquetes que se ejecutan en máquinas PowerPC
</para>
</listitem>
<listitem>
<para>
binary-arm/, para paquetes que se ejecutan en máquinas ARM.
</para>
</listitem>
<listitem>
<para>
binary-hurd-i386/, para paquetes de GNU/Hurd que se ejecutan en máquinas
80x86.
</para>
</listitem>
</itemizedlist>
</section>

<section id="s5.5.3"><title>¿Dónde está el código fuente?</title>
<para>
Se incluye código fuente para cualquier cosa en el sistema Debian.  La mayor
parte de los términos de licencia de los programas en el sistema
<emphasis>exigen</emphasis> que se distribuya el código fuente junto con los
programas, o que se incluya junto con los programas una oferta para entregar el
código fuente.
</para>
<para>
Normalmente, el código fuente se distribuye en los directorios "source", que
son paralelos a todos los directorios binary de cada arquitectura.
</para>
<para>
El código fuente de los paquetes en los directorios "contrib" y "non-free"
puede estar disponible o no, ya que formalmente no son parte del sistema
Debian.
</para>
</section>

</section>

</chapter>

