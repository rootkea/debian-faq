<?xml version='1.0' encoding='utf-8'?>
<!-- -*- DocBook -*- -->
<!DOCTYPE book PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
    "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
    <!ENTITY % shareddata SYSTEM "../debian-faq.ent" > %shareddata;
]>

<chapter id="uptodate"><title>Keeping your Debian system up-to-date</title>
<para>
One of Debian's goals is to provide a consistent upgrade path and a secure
upgrade process.  We always do our best to make upgrading to new releases a
smooth procedure.  In case there's some important note to add to the upgrade
process, the packages will alert the user, and often provide a solution to a
possible problem.
</para>
<para>
You should also read the Release Notes document that describes the details of
specific upgrades.  It is available on the Debian website at <ulink url="&url-debian-releasenotes;"/>
and is also shipped on the Debian CDs, DVDs and Blu-ray discs.
</para>
<section id="howtocurrent"><title>How can I keep my Debian system current?</title>
<para>
One could simply visit a Debian archive site, then peruse
the directories until one finds the desired file, and then fetch it, and
finally install it using <literal>dpkg</literal>.  Note that
<literal>dpkg</literal> will install upgrade files in place, even on a running
system.  Sometimes, a revised package will require the installation of a newly
revised version of another package, in which case the installation will fail
until/unless the other package is installed.
</para>
<para>
Many people find this approach much too time-consuming, since Debian evolves so
quickly -- typically, a dozen or more new packages are uploaded every week.
This number is larger just before a new major release.  To deal with this
avalanche, many people prefer to use a more automated method.  Several
different packages are available for this purpose:
</para>
<section id="aptitude-upgrade"><title>aptitude</title>
<para>
<command>aptitude</command> is the recommended package manager for &debian;
systems, and is described in <xref linkend="aptitude"/>.
</para>
<para>
Before you can use <command>aptitude</command> to make an upgrade, you'll have
to edit the <literal>/etc/apt/sources.list</literal> file to set it up.  If you
wish to upgrade to the latest stable version of Debian, you'll probably want to
use a source like this one:
</para>
<screen>
http://ftp.us.debian.org/debian stable main contrib
</screen>
<para>
You can replace ftp.us.debian.org (the mirror in the United States) with the
name of a faster Debian mirror near you.  See the mirror list at <ulink url="https://www.debian.org/mirror/list"/>
for more information.
</para>
<para>
Or you can use the redirector service httpredir.debian.org which aims to solve
the problem of choosing a Debian mirror.  It uses the geographic location of
the user and other information to choose the best mirror that can serve the
files.  To take advantage of it use a source like this one:
</para>
<screen>
http://httpredir.debian.org/debian stable main contrib
</screen>
<para>
More details on this can be found in the
<citerefentry><refentrytitle>sources.list</refentrytitle><manvolnum>5</manvolnum></citerefentry>
manual page.
</para>
<para>
To update your system from the command line, run
</para>
<screen>
aptitude update
</screen>
<para>
followed by
</para>
<screen>
aptitude full-upgrade
</screen>
<para>
Answer any questions that might come up, and your system will be upgraded.
</para>
<para>
Note that <command>aptitude</command> is not the recommended tool for doing
upgrades from one &debian; release to another.  Use
<command>apt-get</command> instead.  For upgrades between releases you should
read the <ulink
url="&url-debian-releasenotes;">Release Notes</ulink>.
This document describes in detail the recommended steps for upgrades from
previous releases as well as known issues you should consider before upgrading.
</para>
<para>
For details, see the manual page
<citerefentry><refentrytitle>aptitude</refentrytitle><manvolnum>8</manvolnum></citerefentry>,
and the file <filename>/usr/share/aptitude/README</filename>.
</para>
</section>

<section id="apt"><title>apt-get and apt-cdrom</title>
<para>
An alternative to <command>aptitude</command> is <command>apt-get</command>
which is an APT-based command-line tool (described previously in <xref
linkend="apt-get"/>).
</para>
<para>
<command>apt-get</command>, the APT-based command-line tool for handling
packages, provides a simple, safe way to install and upgrade packages.
</para>
<para>
To use <command>apt-get</command>, edit the
<literal>/etc/apt/sources.list</literal> file to set it up, just as for <xref
linkend="aptitude-upgrade"/>.
</para>
<para>
Then run
</para>
<screen>
apt-get update
</screen>
<para>
followed by
</para>
<screen>
apt-get dist-upgrade
</screen>
<para>
Answer any questions that might come up, and your system will be upgraded.  See
also the
<citerefentry><refentrytitle>apt-get</refentrytitle><manvolnum>8</manvolnum></citerefentry>
manual page, as well as <xref linkend="apt-get"/>.
</para>
<para>
If you want to use CDs/DVDs/BDs to install packages, you can use
<command>apt-cdrom</command>.  For details, please see the Release Notes,
section "Adding APT sources from optical media".
</para>
<para>
Please note that when you get and install the packages, you'll still have them
kept in your /var directory hierarchy.  To keep your partition from
overflowing, remember to delete extra files using <literal>apt-get
clean</literal> and <literal>apt-get autoclean</literal>, or to move them
someplace else (hint: use <systemitem role="package">apt-move</systemitem>).
</para>
</section>

</section>

<section id="upgradesingle"><title>Must I go into single user mode in order to upgrade a package?</title>
<para>
No.  Packages can be upgraded in place, even in running systems.  Debian has a
<literal>start-stop-daemon</literal> program that is invoked to stop, then
restart running process if necessary during a package upgrade.
</para>
</section>

<section id="savedebs"><title>Do I have to keep all those .deb archive files on my disk?</title>
<para>
No.  If you have downloaded the files to your disk then after you have
installed the packages, you can remove them from your system, e.g. by running
<literal>aptitude clean</literal>.
</para>
</section>

<section id="keepingalog"><title>How can I keep a log of the packages I added to the system? I'd like to know when upgrades and removals have occurred and on which packages!</title>
<para>
Passing the <literal>--log</literal>-option to <command>dpkg</command> makes
<command>dpkg</command> log status change updates and actions.  It logs both
the <command>dpkg</command>-invokation (e.g.
</para>
<screen>
2005-12-30 18:10:33 install hello 1.3.18 2.1.1-4
</screen>
<para>
) and the results (e.g.
</para>
<screen>
2005-12-30 18:10:35 status installed hello 2.1.1-4
</screen>
<para>
) If you'd like to log all your <command>dpkg</command> invocations (even those
done using frontends like <command>aptitude</command>), you could add
</para>
<screen>
log /var/log/dpkg.log
</screen>
<para>
to your <filename>/etc/dpkg/dpkg.cfg</filename>.  Be sure the created logfile
gets rotated periodically.  If you're using <command>logrotate</command>, this
can be achieved by creating a file <filename>/etc/logrotate.d/dpkg</filename>
with the following lines
</para>
<screen>
/var/log/dpkg {
  missingok
  notifempty
}
</screen>
<para>
More details on <command>dpkg</command> logging can be found in the
<citerefentry><refentrytitle>dpkg</refentrytitle><manvolnum>1</manvolnum></citerefentry>
manual page.
</para>
<para>
<command>aptitude</command> logs the package installations, removals, and
upgrades that it intends to perform to <filename>/var/log/aptitude</filename>.
Note that the <emphasis>results</emphasis> of those actions are not recorded in
this file!
</para>
<para>
Another way to record your actions is to run your package management session
within the
<citerefentry><refentrytitle>script</refentrytitle><manvolnum>1</manvolnum></citerefentry>
program.
</para>
</section>

<section id="autoupdate"><title>Can I automatically update the system?</title>
<para>
Yes.  You can use <command>cron-apt</command>; this tool updates the system at
regular intervals using a cron job.  By default it just updates the package
list and downloads new packages, but without installing them.
</para>
<para>
Note: Automatic upgrade of packages is <emphasis role="strong">NOT</emphasis>
recommended in <emphasis>testing</emphasis> or <emphasis>unstable</emphasis>
systems as this might bring unexpected behaviour and remove packages without
notice.
</para>
</section>

<section id="aptcacher"><title>I have several machines; how can I download the updates only one time?</title>
<para>
If you have more than one Debian machine on your network, it is useful to use
<command>apt-cacher</command> to keep all of your Debian systems up-to-date.
</para>
<para>
<command>apt-cacher</command> reduces the bandwidth requirements of Debian
mirrors by restricting the frequency of Packages, Releases and Sources file
updates from the back end and only doing a single fetch for any file,
independently of the actual request from the proxy.
<command>apt-cacher</command> automatically builds a Debian HTTP mirror based
on requests which pass through the proxy.
</para>
<para>
Of course, you can get the same benefit if you are already using a standard
caching proxy and all your systems are configured to use it.
</para>
</section>

</chapter>

