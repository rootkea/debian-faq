<?xml version='1.0' encoding='utf-8'?>
<!-- -*- DocBook -*- -->
<!DOCTYPE book PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
    "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
    <!ENTITY % shareddata SYSTEM "../debian-faq.ent" > %shareddata;
]>

<chapter id="pkgtools"><title>The Debian package management tools</title>
<section id="pkgprogs"><title>What programs does Debian provide for managing its packages?</title>
<para>
There are multiple tools that are used to manage Debian packages, from graphic
or text-based interfaces to the low level tools used to install packages.  All
the available tools rely on the lower level tools to properly work and are
presented here in decreasing complexity level.
</para>
<para>
It is important to understand that the higher level package management tools
such as <command>aptitude</command> or <command>synaptic</command> rely on
<command>apt</command> which, itself, relies on <command>dpkg</command> to
manage the packages in the system.
</para>
<para>
See <ulink
url="https://www.debian.org/doc/manuals/debian-reference/ch02.en.html">Chapter
2.  Debian package management</ulink> of the <ulink
url="&url-debian-reference;">Debian
reference</ulink> for more information about the Debian package management
utilities.  This document is available in various languages and formats, see
<ulink url="https://www.debian.org/doc/user-manuals#quick-reference">the Debian
Reference entry in the DDP Users' Manuals overview</ulink>.
</para>
<section id="dpkg"><title>dpkg</title>
<para>
This is the main package management program.  <command>dpkg</command> can be
invoked with many options.  Some common uses are:
</para>
<itemizedlist>
<listitem>
<para>
Find out all the options: <literal>dpkg --help</literal>.
</para>
</listitem>
<listitem>
<para>
Print out the control file (and other information) for a specified package:
<literal>dpkg --info foo_VVV-RRR.deb</literal>.
</para>
</listitem>
<listitem>
<para>
Install a package (including unpacking and configuring) onto the file system of
the hard disk: <literal>dpkg --install foo_VVV-RRR.deb</literal>.
</para>
</listitem>
<listitem>
<para>
Unpack (but do not configure) a Debian archive into the file system of the hard
disk: <literal>dpkg --unpack foo_VVV-RRR.deb</literal>.  Note that this
operation does <emphasis>not</emphasis> necessarily leave the package in a
usable state; some files may need further customization to run properly.  This
command removes any already-installed version of the program and runs the
preinst (see <xref linkend="maintscripts"/>) script associated with the
package.
</para>
</listitem>
<listitem>
<para>
Configure a package that already has been unpacked: <literal>dpkg --configure
foo</literal>.  Among other things, this action runs the postinst (see <xref
linkend="maintscripts"/>) script associated with the package.  It also updates
the files listed in the <literal>conffiles</literal> for this package.  Notice
that the 'configure' operation takes as its argument a package name (e.g.,
foo), <emphasis>not</emphasis> the name of a Debian archive file (e.g.,
foo_VVV-RRR.deb).
</para>
</listitem>
<listitem>
<para>
Extract a single file named "blurf" (or a group of files named "blurf*") from a
Debian archive: <literal>dpkg --fsys-tarfile foo_VVV-RRR.deb | tar -xf -
'blurf*'</literal>.
</para>
</listitem>
<listitem>
<para>
Remove a package (but not its configuration files): <literal>dpkg --remove
foo</literal>.
</para>
</listitem>
<listitem>
<para>
Remove a package (including its configuration files): <literal>dpkg --purge
foo</literal>.
</para>
</listitem>
<listitem>
<para>
List the installation status of packages containing the string (or regular
expression) "foo*": <literal>dpkg --list 'foo*'</literal>.
</para>
</listitem>
</itemizedlist>
</section>

<section id="apt-get"><title>APT</title>
<para>
APT is the <emphasis>Advanced Package Tool</emphasis>, an advanced interface to
the Debian packaging system which provides the <command>apt-get</command>
program.  It provides commandline tools for searching and managing packages,
and for querying information about them, as well as low-level access to all
features of the libapt-pkg library.  For more information, see the User's Guide
in <literal>/usr/share/doc/apt-doc/guide.html/index.html</literal> (you will
have to install the <systemitem role="package">apt-doc</systemitem> package).
</para>
<para>
Starting with Debian Jessie, some frequently used <command>apt-get</command>
and <command>apt-cache</command> commands have an equivalent via the new
<command>apt</command> binary.  This means some popular commands like
<command>apt-get update</command>, <command>apt-get install</command>,
<command>apt-get remove</command>, <command>apt-cache search</command>, or
<command>apt-cache show</command> now can also be called simply via
<command>apt</command>, say <command>apt update</command>, <command>apt
install</command>, <command>apt remove</command>, <command>apt
search</command>, or <command>apt show</command>.  The following is an overview
of the old and new commands:
</para>
<screen>
 apt-get update             ->  apt update
 apt-get upgrade            ->  apt upgrade
 apt-get dist-upgrade       ->  apt full-upgrade
 apt-get install package    ->  apt install package
 apt-get remove package     ->  apt remove package
 apt-get autoremove         ->  apt autoremove
 apt-cache search string    ->  apt search string
 apt-cache policy package   ->  apt list -a package
 apt-cache show package     ->  apt show package
 apt-cache showpkg package  ->  apt show -a package
</screen>
<para>
The <command>apt</command> tool merges functionality of apt-get and apt-cache
and by default has a fancier colored output format, making it more pleasant for
humans.  For usage in scripts or advanced use cases, apt-get is still
preferable or needed.
</para>
<para>
<command>apt-get</command> provides a simple way to retrieve and install
packages from multiple sources using the command line.  Unlike
<command>dpkg</command>, <command>apt-get</command> does not understand .deb
files, it works with the packages proper name and can only install .deb
archives from a source specified in <filename>/etc/apt/sources.list</filename>.
<command>apt-get</command> will call <command>dpkg</command> directly after
downloading the .deb archives<footnote><para> Notice that there are ports that
make this tool available with other package management systems, like Red Hat
package manager, also known as <command>rpm</command> </para> </footnote> from
the configured sources.
</para>
<para>
Some common ways to use <command>apt-get</command> are:
</para>
<itemizedlist>
<listitem>
<para>
To update the list of packages known by your system, you can run:
</para>
<screen>
apt update
</screen>
<para>
(you should execute this regularly to update your package lists)
</para>
</listitem>
<listitem>
<para>
To install the <replaceable>foo</replaceable> package and all its dependencies,
run:
</para>
<screen>
apt install foo
</screen>
</listitem>
<listitem>
<para>
To remove the foo package from your system, run:
</para>
<screen>
apt remove foo
</screen>
</listitem>
<listitem>
<para>
To remove the foo package and its configuration files from your system, run:
</para>
<screen>
apt purge foo
</screen>
</listitem>
<listitem>
<para>
To list all packages for which newer versions are available, run:
</para>
<screen>
apt list --upgradable
</screen>
</listitem>
<listitem>
<para>
To upgrade all the packages on your system (without installing extra packages
or removing packages), run:
</para>
<screen>
apt upgrade
</screen>
</listitem>
<listitem>
<para>
To upgrade all the packages on your system, and, if needed for a package
upgrade, installing extra packages or removing packages, run:
</para>
<screen>
apt full-upgrade
</screen>
<para>
(The command <literal>upgrade</literal> keeps a package at its installed
obsolete version if upgrading would need an extra package to be installed, for
a new dependency to be satisfied.  The <literal>full-upgrade</literal> command
is less conservative.)
</para>
</listitem>
</itemizedlist>
<para>
Note that you must be logged in as root to perform any commands that modify
packages.
</para>
<para>
Note that <command>apt-get</command> now also installs recommended packages as
default, and thanks to its robustness it's the preferred program for package
management from console to perform system installation and major system
upgrades.
</para>
<para>
The apt tool suite also includes the <command>apt-cache</command> tool to query
the package lists.  You can use it to find packages providing specific
functionality through simple text or regular expression queries and through
queries of dependencies in the package management system.  Some common ways to
use <command>apt-cache</command> are:
</para>
<itemizedlist>
<listitem>
<para>
To find packages whose description contain <replaceable>word</replaceable>:
</para>
<screen>
apt search <replaceable>word</replaceable>
</screen>
</listitem>
<listitem>
<para>
To print the detailed information of a package:
</para>
<screen>
apt show <replaceable>package</replaceable>
</screen>
</listitem>
<listitem>
<para>
To print the packages a given package depends on:
</para>
<screen>
apt-cache depends <replaceable>package</replaceable>
</screen>
</listitem>
<listitem>
<para>
To print detailed information on the versions available for a package and the
packages that reverse-depends on it:
</para>
<screen>
apt-cache showpkg <replaceable>package</replaceable>
</screen>
</listitem>
</itemizedlist>
<para>
For more information, install the <systemitem role="package">apt</systemitem>
package and read
<citerefentry><refentrytitle>apt</refentrytitle><manvolnum>8</manvolnum></citerefentry>,
<citerefentry><refentrytitle>apt-get</refentrytitle><manvolnum>8</manvolnum></citerefentry>,
<citerefentry><refentrytitle>sources.list</refentrytitle><manvolnum>5</manvolnum></citerefentry>
and install the <systemitem role="package">apt-doc</systemitem> package and
read <filename>/usr/share/doc/apt-doc/guide.html/index.html</filename>.
</para>
</section>

<section id="aptitude"><title>aptitude</title>
<para>
<command>aptitude</command> is a package manager for &debian; systems
that provides a frontend to the apt package management infrastructure.
<command>aptitude</command> is a text-based interface using the curses library.
Actions may be performed from a visual interface or from the command-line.
</para>
<para>
<command>aptitude</command> can be used to perform management tasks in a fast
and easy way.  It allows the user to view the list of packages and to perform
package management tasks such as installing, upgrading, and removing packages.
</para>
<para>
<command>aptitude</command> provides the functionality of
<command>apt-get</command>, as well as many additional features:
</para>
<itemizedlist>
<listitem>
<para>
<command>aptitude</command> offers easy access to all versions of a package.
</para>
</listitem>
<listitem>
<para>
<command>aptitude</command> makes it easy to keep track of obsolete software by
listing it under "Obsolete and Locally Created Packages".
</para>
</listitem>
<listitem>
<para>
<command>aptitude</command> includes a fairly powerful system for searching
particular packages and limiting the package display.  Users familiar with
<command>mutt</command> will pick up quickly, as <command>mutt</command> was
the inspiration for the expression syntax.
</para>
</listitem>
<listitem>
<para>
<command>aptitude</command> can be used to install the predefined tasks
available.  For more information see <xref linkend="tasksel"/>.
</para>
</listitem>
<listitem>
<para>
<command>aptitude</command> in full screen mode has <command>su</command>
functionality embedded and can be run by a normal user.  It will call
<command>su</command> (and ask for the root password, if any) when you really
need administrative privileges.
</para>
</listitem>
</itemizedlist>
<para>
You can use <command>aptitude</command> through a visual interface (simply run
<literal>aptitude</literal>) or directly from the command line.  The command
line syntax used is very similar to the one used in <command>apt-get</command>.
For example, to install the <replaceable>foo</replaceable> package, you can run
<literal>aptitude install <replaceable>foo</replaceable></literal>.
</para>
<para>
Note that <command>aptitude</command> is the preferred program for daily
package management from the console.
</para>
<para>
For more information, read the manual page
<citerefentry><refentrytitle>aptitude</refentrytitle><manvolnum>8</manvolnum></citerefentry>
and install the <systemitem role="package">aptitude-doc</systemitem> package.
</para>
</section>

<section id="synaptic"><title>synaptic</title>
<para>
<command>synaptic</command> is a graphical package manager.  It enables you to
install, upgrade and remove software packages in a user friendly way.  Along
with most of the features offered by aptitude, it also has a feature for
editing the list of used repositories, and supports browsing all available
documentation related to a package.  See the <ulink
url="https://www.nongnu.org/synaptic/">Synaptic Website</ulink> for more
information.
</para>
</section>

<section id="tasksel"><title>tasksel</title>
<para>
When you want to perform a specific task it might be difficult to find the
appropiate suite of packages that fill your need.  The Debian developers have
defined <literal>tasks</literal>, a task is a collection of several individual
Debian packages all related to a specific activity.  Tasks can be installed
through the <command>tasksel</command> program or through
<command>aptitude</command>.
</para>
<para>
Typically, the Debian installer will automatically install the task associated
with a standard system and a desktop environment.  The specific desktop
environment installed will depend on the CD/DVD media used, most commonly it
will be the GNOME desktop (<literal>gnome-desktop</literal> task).  Also,
depending on your selections throughout the installation process, tasks might
be automatically installed in your system.  For example, if you selected a
language other than English, the task associated with it will be installed
automatically too.
</para>
</section>

<section id="dpkg-extra"><title>Other package management tools</title>
<section id="dpkg-deb"><title>dpkg-deb</title>
<para>
This program manipulates Debian archive (<literal>.deb</literal>) files.  Some
common uses are:
</para>
<itemizedlist>
<listitem>
<para>
Find out all the options: <literal>dpkg-deb --help</literal>.
</para>
</listitem>
<listitem>
<para>
Determine what files are contained in a Debian archive file: <literal>dpkg-deb
--contents foo_VVV-RRR.deb</literal>)
</para>
</listitem>
<listitem>
<para>
Extract the files contained in a named Debian archive into a user specified
directory: <literal>dpkg-deb --extract foo_VVV-RRR.deb tmp</literal> extracts
each of the files in <literal>foo_VVV-RRR.deb</literal> into the directory
<literal>tmp/</literal>.  This is convenient for examining the contents of a
package in a localized directory, without installing the package into the root
file system.
</para>
</listitem>
<listitem>
<para>
Extract the control information files from a package: <literal>dpkg-deb
--control foo_VVV-RRR.deb tmp</literal>.
</para>
</listitem>
</itemizedlist>
<para>
Note that any packages that were merely unpacked using <literal>dpkg-deb
--extract</literal> will be incorrectly installed, you should use <literal>dpkg
--install</literal> instead.
</para>
<para>
More information is given in the manual page
<citerefentry><refentrytitle>dpkg-deb</refentrytitle><manvolnum>1</manvolnum></citerefentry>.
</para>
</section>

</section>

</section>

<section id="updaterunning"><title>Debian claims to be able to update a running program; how is this accomplished?</title>
<para>
The kernel (file system) in &debian; systems supports replacing files
even while they're being used.
</para>
<para>
We also provide a program called <command>start-stop-daemon</command> which is
used to start daemons at boot time or to stop daemons when the runlevel is
changed (e.g., from multi-user to single-user or to halt).  The same program is
used by installation scripts when a new package containing a daemon is
installed, to stop running daemons, and restart them as necessary.
</para>
</section>

<section id="whatpackages"><title>How can I tell what packages are already installed on a Debian system?</title>
<para>
To learn the status of all the packages installed on a Debian system, execute
the command
</para>
<screen>
dpkg --list
</screen>
<para>
This prints out a one-line summary for each package, giving a 2-letter status
symbol (explained in the header), the package name, the version which is
<emphasis>installed</emphasis>, and a brief description.
</para>
<para>
To learn the status of packages whose names match any pattern beginning with
"foo", run the command:
</para>
<screen>
dpkg --list 'foo*'
</screen>
<para>
To get a more verbose report for a particular package, execute the command:
</para>
<screen>
dpkg --status packagename
</screen>
</section>

<section id="listfiles"><title>How do I display the files of an installed package?</title>
<para>
To list all the files provided by the installed package <literal>foo</literal>
execute the command
</para>
<screen>
dpkg --listfiles foo
</screen>
<para>
Note that the files created by the installation scripts aren't displayed.
</para>
</section>

<section id="filesearch"><title>How can I find out what package produced a particular file?</title>
<para>
To identify the package that produced the file named <literal>foo</literal>
execute either:
</para>
<itemizedlist>
<listitem>
<para>
<literal>dpkg --search foo</literal>
</para>
<para>
This searches for <literal>foo</literal> in installed packages.  (This is
(currently) equivalent to searching all of the files having the file extension
of <literal>.list</literal> in the directory
<literal>/var/lib/dpkg/info/</literal>, and adjusting the output to print the
names of all the packages containing it, and diversions.)
</para>
<para>
A faster alternative to this is the <command>dlocate</command> tool.
</para>
<screen>
dlocate -S foo
</screen>
</listitem>
<listitem>
<para>
<literal>zgrep foo Contents-ARCH.gz</literal>
</para>
<para>
This searches for files which contain the substring <literal>foo</literal> in
their full path names.  The files <literal>Contents-ARCH.gz</literal> (where
ARCH represents the wanted architecture) reside in the major package
directories (main, non-free, contrib) at a Debian archive site (i.e. under
<literal>/debian/dists/&releasename;</literal>).  A <literal>Contents</literal> file
refers only to the packages in the subdirectory tree where it resides.
Therefore, a user might have to search more than one
<literal>Contents</literal> files to find the package containing the file
<literal>foo</literal>.
</para>
<para>
This method has the advantage over <literal>dpkg --search</literal> in that it
will find files in packages that are not currently installed on your system.
</para>
</listitem>
<listitem>
<para>
<literal>apt-file search <replaceable>foo</replaceable></literal>
</para>
<para>
If you install the <systemitem role="package">apt-file</systemitem> package,
similar to the above, it searches files which contain the substring or regular
expression <literal>foo</literal> in their full path names.  The advantage over
the example above is that there is no need to retrieve the
<literal>Contents-ARCH.gz</literal> files as it will do this automatically for
all the sources defined in <filename>/etc/apt/sources.list</filename> when you
run (as root) <literal>apt-file update</literal>.
</para>
</listitem>
</itemizedlist>
</section>

<section id="datapackages"><title>Why is `foo-data' not removed when I uninstall `foo'? How do I make sure old unused library-packages get purged?</title>
<para>
Some packages are split in program (`foo') and data (`foo-data') (or in `foo'
and `foo-doc').  This is true for many games, multimedia applications and
dictionaries in Debian and has been introduced since some users might want to
access the raw data without installing the program or because the program can
be run without the data itself, making `foo-data' optional.
</para>
<para>
Similar situations occur when dealing with libraries: generally these get
installed since packages containing applications depend on them.  When the
application-package is purged, the library-package might stay on the system.
Or: when the application-package no longer depends upon e.g. libdb4.2, but
upon libdb4.3, the libdb4.2 package might stay when the application-package is
upgraded.
</para>
<para>
In these cases, `foo-data' doesn't depend on `foo', so when you remove the
`foo' package it will not get automatically removed by most package management
tools.  The same holds true for the library packages.  This is necessary to
avoid circular dependencies.  However, if you use <command>apt-get</command>
(see <xref linkend="apt-get"/>) or <command>aptitude</command> (see <xref
linkend="aptitude"/>) as your package management tool, they will track
automatically installed packages and give the possibility to remove them, when
no packages making use of them remain in your system.
</para>
</section>

</chapter>

