<?xml version='1.0' encoding='utf-8'?>
<!-- -*- DocBook -*- -->
<!DOCTYPE book PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
    "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
    <!ENTITY % shareddata SYSTEM "../debian-faq.ent" > %shareddata;
]>

<chapter id="kernel"><title>Debian and the kernel</title>
<section id="non-debian-kernel"><title>Can I install and compile a kernel without some Debian-specific tweaking?</title>
<para>
Yes.
</para>
<para>
There's only one common catch: the Debian C libraries are built with the most
recent <emphasis>stable</emphasis> releases of the <emphasis
role="strong">kernel</emphasis> headers.  If you happen to need to compile a
program with kernel headers newer than the ones from the stable branch, then
you should either upgrade the package containing the headers (<systemitem
role="package">linux-libc-dev</systemitem>), or use the new headers from an
unpacked tree of the newer kernel.  That is, if the kernel sources are in
<filename>/usr/src/linux</filename>, then you should add
<literal>-I/usr/src/linux/include/</literal> to your command line when
compiling.
</para>
</section>

<section id="customkernel"><title>What tools does Debian provide to build custom kernels?</title>
<para>
Users who wish to (or must) build a custom kernel are encouraged to use the
Debian package target included with recent versions of the kernel build system.
After configuring the kernel, simply run the following command:
</para>
<screen>
make deb-pkg
</screen>
<para>
The new kernel package will be created in the directory one level above the
kernel source tree, and it may be installed using <literal>dpkg -i</literal>.
</para>
<para>
Users must separately download the source code for the most recent kernel (or
the kernel of their choice) from their favorite Linux archive site, unless a
<systemitem
role="package">linux-source-<replaceable>version</replaceable></systemitem>
package is available (where <replaceable>version</replaceable> stands for the
kernel version).
</para>
</section>

<section id="modules"><title>What special provisions does Debian provide to deal with modules?</title>
<para>
A configuration file containing modules to be manually loaded at boot time is
kept at <filename>/etc/modules</filename>.  However, editing this file is
rarely needed.
</para>
<para>
Other module configuration is kept in the <filename>/etc/modprobe.d/</filename>
directory.  More information about the format of those files can be found in
the
<citerefentry><refentrytitle>modprobe.conf</refentrytitle><manvolnum>5</manvolnum></citerefentry>
manual page.
</para>
</section>

<section id="removeoldkernel"><title>Can I safely de-install an old kernel package, and if so, how?</title>
<para>
Yes.  The <command>linux-image-<replaceable>NNN</replaceable>.prerm</command>
script checks to see whether the kernel you are currently running is the same
as the kernel you are trying to de-install.  Therefore you can remove unwanted
kernel image packages using this command:
</para>
<screen>
dpkg --purge linux-image-<replaceable>NNN</replaceable>
</screen>
<para>
(replace <replaceable>NNN</replaceable> with your kernel version and revision
number, of course)
</para>
</section>

<section id="moreinfo"><title>Where can I get more information about Linux packages for Debian?</title>
<para>
Further information is maintained in the <ulink
url="https://kernel-team.pages.debian.net/kernel-handbook/">Debian Linux Kernel
Handbook</ulink>.
</para>
</section>

</chapter>

