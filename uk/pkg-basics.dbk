<?xml version='1.0' encoding='utf-8'?>
<!-- -*- DocBook -*- -->
<chapter id="pkg-basics"><title>Основи системи управління пакунками Debian</title>
<section id="package"><title>Що таке пакунок Debian?</title>
<para>
Пакунок зазвичай містить всі файли, що
потрібні для реалізації певних команд чи
можливостей.  Є два типи пакунків Debian:
</para>
<itemizedlist>
<listitem>
<para>
<emphasis>Двійкові пакунки</emphasis>, що містять
виконувані і конфігураційні файли,
сторінки довідки info/man, інформацію про
авторські права та іншу документацію.  Ці
пакунки поширюються у специфічному для Debian
архівному форматі (див.  <xref linkend="deb-format"/>); як
правило вони вирізняються закінченням
„.deb“.  Двійкові пакунки можуть бути
розпаковані за допомогою спеціального
інструменту — <literal>dpkg</literal>; деталі
описано в його довідці.
</para>
</listitem>
<listitem>
<para>
<emphasis>Джерельні пакунки</emphasis>, що містять
файл <literal>.dsc</literal>, котрий описує пакунок
(включаючи назви наступних файлів), файл
<literal>.orig.tar.gz</literal>, що містить оригінальний,
незмінений джерельний код у форматі tar,
стиснутому gzip'ом, та зазвичай
<literal>.diff.gz</literal> файл, що містить специфічні
для Debian зміни джерельного коду.  Утиліта
<literal>dpkg-source</literal> запаковує та розпаковує
джерельні архіви Debian; деталі описано в її
довідці.
</para>
</listitem>
</itemizedlist>
<para>
Встановлення програмного забезпечення за
допомогою системи керування пакунками
використовує „залежності“, які старанно
формуються супроводжуючими пакунків.  Ці
залежності описані у файлі <literal>control</literal>,
що знаходиться в кожному пакунку.
Наприклад, пакунок, що містить компілятор
GCU C (<systemitem role="package">gcc</systemitem>) „залежить“
від пакунка <systemitem role="package">binutils</systemitem>,
котрий містить зв'язувач (linker) та асемблер.
Якщо користувач спробує встановити <systemitem
role="package">gcc</systemitem> не маючи при цьому
встановленого <systemitem role="package">binutils</systemitem>,
система керування пакунками (dpkg) надрукує
повідомлення про помилку та перерве
встановлення <systemitem role="package">gcc</systemitem>
(однак, наполегливий користувач може
скорегувати таку поведінку; див.
<citerefentry><refentrytitle>dpkg</refentrytitle><manvolnum>8</manvolnum></citerefentry>).
Щоб дізнатись більше, перегляньте <xref
linkend="depends"/> нижче.
</para>
<para>
Пакувальні інструменти Debian можуть
вживатись для:
</para>
<itemizedlist>
<listitem>
<para>
маніпуляцій та керування пакунками чи
частинами пакунків,
</para>
</listitem>
<listitem>
<para>
допомоги користувачу у разі необхідності
розділення пакунків, наприклад для
копіювання на дискети,
</para>
</listitem>
<listitem>
<para>
допомоги розробникам у формуванні архівів
пакунків, та
</para>
</listitem>
<listitem>
<para>
допомоги користувачам у встановленні
пакунків, що розташовані на віддаленому
ftp-сайті.
</para>
</listitem>
</itemizedlist>
</section>

<section id="deb-format"><title>Який формат двійкових пакунків Debian?</title>
<para>
Пакунок, або ж архівний файл Debian містить
виконувані файли, бібліотеки та
документацію, що відносяться до певного
набору відповідних програм.  Зазвичай
назва такого файлу закінчується на
<literal>.deb</literal>.
</para>
<para>
Нутрощі формату двійкових пакунків Debian
описані у сторінці довідки
<citerefentry><refentrytitle>deb</refentrytitle><manvolnum>5</manvolnum></citerefentry>.
Внутрішній формат є предметом змін (між
основними версіями Debian GNU/Linux), тому, будь
ласка, завжди використовуйте
<citerefentry><refentrytitle>dpkg-deb</refentrytitle><manvolnum>1</manvolnum></citerefentry>
для маніпуляцій з <literal>.deb</literal>-файлами.
</para>
</section>

<section id="pkgname"><title>Чому назви пакунків Debian такі довгі?</title>
<para>
Назва двійкового Debian-пакунку формується за
наступною домовленістю:
&lt;Дещо&gt;_&lt;НомерВерсії&gt;-&lt;НомерРедакціїВDebian&gt;.deb
</para>
<para>
Мається на увазі, що <literal>дещо</literal>
насправді є назвою пакунка.  Для перевірки
ви можете дізнатись назву пакунка, що
пов'язаний файлом архіву Debian одним з двох
шляхів:
</para>
<itemizedlist>
<listitem>
<para>
перевірити файл Packages в теці, в котрій він
розміщувався на ftp-сайті Debian.  Цей файл
містить опис кожного пакунку; в тому числі
й формальна назва пакунка.
</para>
</listitem>
<listitem>
<para>
за допомогою команди <literal>dpkg --info
foo_VVV-RRR.deb</literal> (де VVV та RRR — версія та
редакція пакунка відповідно).  Вона
відображає, поміж інших речей, ще й
відповідну назву пакунка для даного
архіву.
</para>
</listitem>
</itemizedlist>
<para>
Компонента <literal>VVV</literal> — це номер версії,
що визначається основним розробником.  Він
не підкоряється жодним стандартам і може
мати різні формати, як „19990513“ так і
„1.3.8pre1“.
</para>
<para>
Компонента <literal>RRR</literal> — це номер
редакції пакунка в Debian, він визначається
розробником (чи окремим користувачем, якщо
він захоче сформувати пакунок самостійно).
Цей номер вказує на кількість редагувань
пакунка; таким чином новий рівень редакції
як правило означає зміни у Makefile
(<literal>debian/rules</literal>), контролюючому файлі
(<literal>debian/control)</literal>, встановлювальному чи
видаляючому сценаріях(<literal>debian/p*</literal>), або
ж у конфігураційних файлах, що
використовується пакунком.
</para>
</section>

<section id="controlfile"><title>Що то за файл control?</title>
<para>
Детальний опис вмісту файлу control можна
знайти в 5-му розділі Підручника політики
Debian, див.  <xref linkend="debiandocs"/>.
</para>
<para>
Тут ми коротко розглянемо приклад файлу
control пакунка hello:
</para>
<screen>
Package: hello
Priority: optional
Section: devel
Installed-Size: 45
Maintainer: Adam Heath &lt;doogie@debian.org&gt;
Architecture: i386
Version: 1.3-16
Depends: libc6 (>= 2.1)
Description: The classic greeting, and a good example
 The GNU hello program produces a familiar, friendly greeting.  It
 allows nonprogrammers to use a classic computer science tool which
 would otherwise be unavailable to them.
 .
 Seriously, though: this is an example of how to do a Debian package.
 It is the Debian version of the GNU Project's 'hello world' program
 (which is itself an example for the GNU Project).
</screen>
<para>
У полі Package вказується назва пакунка.  Саме
за ним пакунком можна маніпулювати за
допомогою спеціальних інструментів, і вона
як правило (але не обов'язково) є такою ж, як
і перша частина назви архівного файлу Debian.
</para>
<para>
У полі Version вказується як версія виробника
програми, так і рівень редакції пакунка
Debian, як це описано в <xref linkend="pkgname"/>.
</para>
<para>
Поле Architecture вказує, для якої системи
скомпільований двійковий файл.
</para>
<para>
В полі Depends перелічуються пакунки, котрі
повинні бути встановлені для успішного
встановлення даного пакунку.
</para>
<para>
Поле Installed-Size вказує, скільки дискового
простору займе встановлений пакунок.
Передбачається, що його будуть
використовувати інші програми для того,
щоб визначити, чи достатньо на розділі
місця для встановлення пакунка.
</para>
<para>
Рядок Section вказує на розділ, в котрому
міститься пакунок на ftp-сайтах Debian.  Це —
назва підтеки (в одній з основних тек —
див.  <xref linkend="dirtree"/>), де розташовано
пакунок.
</para>
<para>
Priority вказує на те, наскільки даний пакунок
важливий при встановленні, то ж
напівінтелектуальні програми на кшталт
dselect чи console-apt можуть поділити пакунки на
категорії, наприклад за ступенем
необхідності їх встановлення.  Див.  <xref
linkend="priority"/>.
</para>
<para>
В полі Maintainer можна знайти електронну
адресу людини, котра що на даний момент
відповідає за супровід пакунка.
</para>
<para>
В полі Description знаходиться короткий перелік
основних можливостей пакунка.
</para>
<para>
За додатковою інформацією про всі можливі
поля, що може мати пакунок, зверніться до
розділу 5 Підручника політики Debian,
„Контрольні файли та їх поля“.
</para>
</section>

<section id="conffile"><title>Для чого призначені conffile?</title>
<para>
У цьому файлі перелічуються
конфігураційніфайли (розміщених, як
правило, в теці <literal>/etc</literal>), котрі система
керування пакунками не буде
перезаписувати при оновленні пакунка.
Таким чином досягається захист вмісту
конфігураційних файлів, що є критичним на
працюючій системі.
</para>
<para>
Щоб дізнатись точно, які саме файли не
будуть затерті при оновленні пакунка,
запустіть
</para>
<screen>
dpkg --status package
</screen>
<para>
та перегляньте поле Conffiles.
</para>
</section>

<section id="maintscripts"><title>Що роблять сценарії preinst, postinst, prerm, та postrm?</title>
<para>
Ці файли — це виконувані сценарії, що
автоматично запускаються перед чи після
встановлення пакунку.  Разом з файлом
<literal>control</literal> всі ці файли є частиною
„контролюючої“ секції файлу архіву Debian.
</para>
<para>
Кожен окремо вони роблять наступне:
</para>
<variablelist>
<varlistentry>
<term>preinst</term>
<listitem>
<para>
Цей сценарій запускається перед
розпаковуванням пакунку з його Debian-архіву
(„.deb“-файлу).  Багато preinst-сценаріїв
зупиняють служби для пакунків, що
оновлюються, аж доки їх встановлення чи
оновлення не завершаться (з наступним
успішним виконанням сценарію postinst).
</para>
</listitem>
</varlistentry>
<varlistentry>
<term>postinst</term>
<listitem>
<para>
Цей сценарій, як правило, завершує
необхідні конфігурації пакунка
<literal>foo</literal> коли його вже було розпаковано
з архівного „.deb“-файлу.  Часто postinst
просить ввести користувача якусь
інформацію та/або попереджає його у
випадку використання стандартних значень,
щоб він не забув повернутись та
переконфігурувати пакунок у разі
необхідності.  Потім багато сценаріїв
виконують команди, необхідні для запуску
чи перезапуску різноманітних служб,
оскільки новий пакунок вже встановлено чи
оновлено.
</para>
</listitem>
</varlistentry>
<varlistentry>
<term>prerm</term>
<listitem>
<para>
Цей сценарій як правило зупиняє всі демони,
що мають певне відношення до пакунку.  Він
виконується перед видаленням файлів,
пов'язаних з пакунком.
</para>
</listitem>
</varlistentry>
<varlistentry>
<term>postrm</term>
<listitem>
<para>
Цей сценарій зазвичай змінює відсилачі та
інші файли, пов'язані з <literal>foo</literal> та/або
видаляє файли, створені пакунком (див.
також <xref linkend="virtual"/>).
</para>
</listitem>
</varlistentry>
</variablelist>
<para>
На даний момент всі керуючі файли можна
знайти в теці <literal>/var/lib/dpkg/info</literal>.  Файли,
що відносяться до пакунка <literal>foo</literal>
починаються з foo і мають розширення
відповідно preinst, postinst і т.д.  Файл
<literal>foo.list</literal> з вищевказаної теки — це
список всіх файлів, встановлених з пакунка
<literal>foo</literal>.  (Зауважте, що
місцезнаходження цих файлів визначається
внутрішніми налаштуваннями dpkg; ви не
повинні орієнтуватись саме на цей шлях.)
</para>
</section>

<section id="priority"><title>Що це за пакунки — <emphasis>Essential</emphasis>, <emphasis>Required</emphasis>, <emphasis>Important</emphasis>, <emphasis>Standard</emphasis>, <emphasis>Optional</emphasis> та <emphasis>Extra</emphasis> (Невіддільний, Необхідний, Важливий, Типовий, Необов'язковий та Додатковий)?</title>
<para>
Кожному пакункові Debian супроводжуючими
збірки присвоюється <emphasis>пріоритет</emphasis>,
щоб допомогти системі керування пакунками.
Пріоритети бувають:
</para>
<itemizedlist>
<listitem>
<para>
<emphasis role="strong">Required</emphasis> (Необхідний):
пакунки, що необхідні для правильного
функціонування системи.
</para>
<para>
Сюди входять всі інструменти, необхідні
для усунення дефектів системи.  Ви не
повинні видаляти ці пакунки, інакше ваша
система буде повністю виведена з ладу і
швидше за все ви навіть не зможете
використати dpkg щоб встановити їх назад.  В
системах з набором лише необхідних
пакунків, швидше за все, не можна працювати,
але вони надають системному
адміністратору достатньо
функціональності, щоб завантажити систему
та встановити додаткове програмне
забезпечення.
</para>
</listitem>
<listitem>
<para>
<emphasis role="strong">Important</emphasis> (Важливі) пакунки
повинні бути встановлені на будь-якій
Unix-системі
</para>
<para>
До таких відносяться всі інші пакунки, без
яких використання системи буде незручним
або маломожливим.  Сюди <emphasis>НЕ</emphasis>
входять Emacs, X11, TeX чи інші великі додатки; ці
пакунки лише формують кістяк.
</para>
</listitem>
<listitem>
<para>
<emphasis role="strong">Standard</emphasis> (Типові) пакунки є
стандартом для будь-якої Linux-системи,
включаючи але не обмежуючись текстовими
системами.
</para>
<para>
Сюди входить все те, що встановлюється по
замовчуванню, якщо користувачі не вибрали
що-небудь ще.  Це не стосується багатьох
великих додатків, але все ж до таких
пакунків відносяться деякі програми для
розробників на кшталт компіляторів GNU C та
С++ (<literal>gcc</literal>, <literal>g++</literal>), GNU make, а також
інтерпретатор мови Python, деяке серверне
програмне забезпечення типу OpenSSH, демон
друку BSD (<literal>lpr</literal>) і дзеркало портів
(portmapper) RPC (<literal>portmap</literal>).
</para>
</listitem>
<listitem>
<para>
<emphasis role="strong">Optional</emphasis> (Необов'язкові)
пакунки включають в себе все те, що вам є
сенс встановити, якщо ви не знаєте, що воно
таке, або ж не маєте якихось особливих
забаганок.
</para>
<para>
Сюди входять Х11, повна збірка ТеХ та багато
інших програм.
</para>
</listitem>
<listitem>
<para>
<emphasis role="strong">Extra</emphasis> (Додаткові): пакунки,
котрі або конфліктують з іншими, з вищим
пріоритетом, або мають сенс до
використання, лише якщо ви точно знаєте, що
воно таке, або ж мають якісь спеціалізовані
вимоги, що не дозволили включити їх до Optional.
</para>
</listitem>
</itemizedlist>
<para>
Якщо ви встановлюєте стандартний Debian, всі
пакунки з пріоритетом <emphasis
role="strong">Standard</emphasis> та вищим буде
встановлено до вашої системи.  Якщо ви
оберете перед-визначення задач
встановлення ви отримаєте доступ і до
пакунків з нижчим пріоритетом також.
</para>
<para>
На додаток, деякі пакунки позначено як
<emphasis role="strong">Essential</emphasis> (Невіддільні),
оскільки вони абсолютно необхідні для
успішного функціонування системи.
Інструменти системи керування пакунками
не зможуть їх видалити.
</para>
</section>

<section id="virtual"><title>Що таке віртуальний пакунок?</title>
<para>
Віртуальний пакунок — це загальна назва,
що дається пакункові чи групі пакунків,
кожен з яких пропонує схожу базову
функціональність.  Наприклад, обидві
програми <literal>tin</literal> та <literal>trn</literal>
призначені для читання новин, а отже
повинні задовольнити залежність іншої
програми, що потребує встановленої
програми для читання новин у системі.  Тому
обидві вони забезпечують „віртуальний
пакунок“, що називається <literal>news-reader</literal>.
</para>
<para>
Аналогічно, програми <literal>smail</literal> та
<literal>sendmail</literal> забезпечують
функціональність транспортного агента
електронної пошти.  Тому кажуть, що вони
обидві забезпечують віртуальний пакунок
„mail transport agent“.  Якщо хоча б одна з них
встановлена, будь-яка інша програма, що
залежить від <literal>mail-transport-agent</literal>, буде
задоволена наявністю віртуального
пакунка.
</para>
<para>
Debian забезпечує цей механізм таким чином, що
якщо в системі встановлено більш ніж один
пакунок з однаковою базовою
функціональністю, то системний
адміністратор може зробити один із них
привілейованим.  Для ознайомленням з
описом можливостей відповідної команди,
<literal>update-alternatives</literal>, перегляньте <xref
linkend="diverse"/>.
</para>
</section>

<section id="depends"><title>Що мається на увазі, коли кажуть, що пакунок залежить, рекомендує, пропонує, конфліктує, замінює або забезпечує (<emphasis>Depends</emphasis>, <emphasis>Recommends</emphasis>, <emphasis>Suggests</emphasis>, <emphasis>Conflicts</emphasis>, <emphasis>Replaces</emphasis> або <emphasis>Provides</emphasis>) інший пакунок?</title>
<para>
Система керуваннями пакунків Debian оперує
шкалою залежностей, щоб вказати, в який
спосіб програма А може реагувати на
наявність в системі програми В:
</para>
<itemizedlist>
<listitem>
<para>
Пакунок A залежить (<emphasis>depends</emphasis>) від
пакунку B, якщо B безумовно повинен бути
встановленим у системі щоб запустити
програму A.  В деяких випадках A залежить не
просто від B, але від певної версії B.  В
такому випадку залежність як правило має
нижню межу, в цьому випадку A залежить від
будь-якої версії B, що новіша за деяку
визначену версію.
</para>
</listitem>
<listitem>
<para>
Пакунок A рекомендує (<emphasis>recommends</emphasis>)
пакунок B, якщо супроводжуючий пакунку A
вважає, що переважній більшості
користувачів не потрібен пакунок A без
функціональності, що її забезпечує пакунок
B.
</para>
</listitem>
<listitem>
<para>
Пакунок A пропонує (<emphasis>suggests</emphasis>)
пакунок B, якщо B містить файли, пов'язані з
функціональністю A (та, зазвичай,
покращують її).
</para>
</listitem>
<listitem>
<para>
Пакунок A конфліктує (<emphasis>conflicts</emphasis>) з
пакунком B, якщо A не зможе функціонувати
при встановленому у системі B.  Значно рідше
конфлікт виникає через те, що A містить
файли, відмінні від аналогічних у пакунку B.
Часто conflicts поєднується з replaces.
</para>
</listitem>
<listitem>
<para>
Пакунок A замінює (<emphasis>replaces</emphasis>) пакунок
B, коли файли, встановлені B видаляються та
(в деяких випадках) перезаписуються
файлами з A.
</para>
</listitem>
<listitem>
<para>
Пакунок A забезпечує (<emphasis>provides</emphasis>)
пакунок B, коли всі файли та
функціональність B включені в A.  Цей
механізм дає можливість користувачам з
обмеженим дисковим простором встановити
лише ту частину пакунку A, котра їм справді
необхідна.
</para>
</listitem>
</itemizedlist>
<para>
Детальніша інформація по кожному з цих
термінів знаходиться у підручнику
Політики Debian.
</para>
</section>

<section id="pre-depends"><title>Що означає „попередня залежність“?</title>
<para>
„Попередня залежність“ — це особлива
залежність.  Для більшості пакунків
<literal>dpkg</literal> розпакує архівний
(<literal>.deb</literal>) файл незалежно від того, чи є
в системі файли, від котрих він залежить.
Це означає, що <literal>dpkg</literal> добуде їх з
архіву та помістить туди, де вони повинні
знаходитись.  Якщо ці пакунки
<emphasis>залежать</emphasis> від наявності інших
пакунків у системі, <literal>dpkg</literal>
відмовиться завершити встановлення
(виконуючи дію „configure“), поки інші пакунки
не будуть встановлені.
</para>
<para>
Проте для деяких пакунків <literal>dpkg</literal>
відмовиться проводити розпакування, поки
всі залежності не будуть задоволені.  Цей
механізм називається попередньою
залежністю від присутності у системі інших
пакунків.  Проект Debian пропонує цей механізм
для безпечного оновлення системи від
формату <literal>a.out</literal> до <literal>ELF</literal>, де
<emphasis>порядок</emphasis>, у якому розпаковуються
пакунки, є критичним.  Є й інші випадки
значних оновлень, у яких цей механізм є
доволі зручним, наприклад пакунки з
пріоритетом „необхідний“ та їхня
залежність від LibC.
</para>
<para>
Як завжди, більш детальну інформацію про це
можна знайти в Підручнику політики Debian.
</para>
</section>

<section id="pkgstatus"><title>Що мається на увазі, кажучи невідомий, встановлений, видалений, очищений чи зафіксований (<emphasis>unknown</emphasis>, <emphasis>install</emphasis>, <emphasis>remove</emphasis>, <emphasis>purge</emphasis> та <emphasis>hold</emphasis>) про стан пакунку.</title>
<para>
Ці прапорці вказують, що користувач хоче
зробити з пакунком (як вказано або діями
користувача в секції Select програми
<literal>dselect</literal>, або ж прямими викликами
<literal>dpkg</literal>).
</para>
<para>
Вони означають наступне:
</para>
<itemizedlist>
<listitem>
<para>
unknown (невідомий) — користувач ніколи не
цікавився цим пакунком.
</para>
</listitem>
<listitem>
<para>
install (встановлений) — користувач бажає
встановити чи оновити пакунок.
</para>
</listitem>
<listitem>
<para>
remove (видалений) — користувач хоче видалити
пакунок, але не хотів видаляти існуючі
файли конфігурації.
</para>
</listitem>
<listitem>
<para>
purge (очищений) — користувач хотів би
видалити пакунок разом з усіма
конфігураційними файлами.
</para>
</listitem>
<listitem>
<para>
hold (зафіксований) — користувач не бажає
обробляти цей пакунок, тобто він хоче
зберегти його поточну версію з поточним
статусом.
</para>
</listitem>
</itemizedlist>
</section>

<section id="puttingonhold"><title>Як мені зафіксувати пакунок?</title>
<para>
Є три способи зафіксувати пакунок — за
допомогою dpkg, aptitude, або ж dselect.
</para>
<para>
Для dpkg вам потрібно експортувати список
станів пакунків за допомогою команди
</para>
<screen>
dpkg --get-selections \* > selections.txt
</screen>
<para>
Далі відредагуйте отриманий файл
<filename>selections.txt</filename>, змінивши рядки, що
містять назви пакунків, які ви хочете
зафіксувати, як наприклад <systemitem
role="package">libc6</systemitem> з ось таких
</para>
<screen>
libc6                                           install
</screen>
<para>
на такі:
</para>
<screen>
libc6                                           hold
</screen>
<para>
Збережіть файл та завантажте його назад в
базу даних dpkg таким чином:
</para>
<screen>
dpkg --set-selections < selections.txt
</screen>
<para>
У випадку aptitude ви можете зафіксувати
пакунок такою командою:
</para>
<screen>
aptitude hold назва_пакунка
</screen>
<para>
та зняти фіксацію командою
</para>
<screen>
aptitude unhold назва_пакунка
</screen>
<para>
Для dselect вам потрібно лише перейти до
екрану [S]elect, знайти пакунок, котрий ви
хочете зафіксувати, та натиснути клавішу '='
або 'Н'.  Зміни збережуться, як тільки ви
покинете екран [S]elect.
</para>
</section>

<section id="sourcepkgs"><title>Як мені встановити джерельний пакунок?</title>
<para>
Джерельні пакунки Debian насправді не можуть
бути встановлені, вони лише розпаковуються
в ту теку, де ви хочете зібрати двійкові
пакунки, котрі вони створюють.
</para>
<para>
Джерельні пакунки розповсюджуються на
більшості тих дзеркал, котрі пропонують
двійкові пакунки.  Якщо ви налаштуєте
<citerefentry><refentrytitle>sources.list</refentrytitle><manvolnum>5</manvolnum></citerefentry>
вашого APT таким чином, щоб він включав
необхідні рядки „deb-src“, ви зможете легко
встановлювати будь-які джерельні пакунки
командою
</para>
<screen>
apt-get source назва_пакунка
</screen>
<para>
Щоб дійсно допомогти вам компонувати
джерельні пакунки, Debian пропонує так званий
механізм компонування залежностей.
Мається на увазі, що супроводжуючий
джерельного пакунку веде список інших
пакунків, що потрібні для компонування
даного.  Щоб побачити, як це зручно,
запустіть
</para>
<screen>
apt-get build-dep назва_пакунка
</screen>
<para>
перед збиранням джерельних кодів.
</para>
</section>

<section id="sourcebuild"><title>Як мені скомпонувати двійковий пакунок з джерельного?</title>
<para>
Вам потрібні всі файли foo_*.dsc, foo_*.tar.gz та
foo_*.diff.gz щоб скомпілювати джерельні коди
(зауважте, що для деяких пакунків, що є
рідними для Debian, немає файлів.diff.gz).
</para>
<para>
Після того як ви їх отримали (див.  <xref
linkend="sourcepkgs"/>), якщо у вас встановлено
пакунок <systemitem role="package">dpkg-dev</systemitem>, команда
</para>
<screen>
dpkg-source -x foo_version-revision.dsc
</screen>
<para>
видобуде пакунок в теку під назвою
<literal>foo-version</literal>.
</para>
<para>
Якщо ви хочете лише скомпілювати пакунок,
ви можете перейти в цю теку та запустити
команду
</para>
<screen>
dpkg-buildpackage -rfakeroot -b
</screen>
<para>
щоб скомпонувати пакунок (зауважте, що при
цьому у вас повинен бути встановлений
пакунок <systemitem role="package">fakeroot</systemitem>), а потім
</para>
<screen>
dpkg -i ../foo_version-revision_arch.deb
</screen>
<para>
щоб встановити новозбудований пакунок.
</para>
</section>

<section id="creatingdebs"><title>А як мені створити власний пакунок Debian?</title>
<para>
Для детального опису прочитайте Довідник
нового супроводжуючого, доступний у
пакунку <systemitem role="package">maint-guide</systemitem>, або за
адресою <ulink
url="http://www.debian.org/doc/devel-manuals#maint-guide">http://www.debian.org/doc/devel-manuals#maint-guide</ulink>.
</para>
</section>

</chapter>

